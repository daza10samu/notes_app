from django.shortcuts import render
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView, CreateAPIView

from notes.models import Note, Attachment
from notes.serializer import NoteSerializer, AttachmentCreateSerializer


class NoteListCreate(ListCreateAPIView):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer


class NoteRetrieveUpdateDestroy(RetrieveUpdateDestroyAPIView):
    serializer_class = NoteSerializer
    queryset = Note.objects.all()


class AttachmentCreate(CreateAPIView):
    serializer_class = AttachmentCreateSerializer
    queryset = Attachment.objects.all()
