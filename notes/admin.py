from django.contrib import admin

from notes.models import Note, Attachment


class NoteAttachmentInline(admin.StackedInline):
    model = Attachment
    extra = 1


class NoteAdmin(admin.ModelAdmin):
    inlines = [
        NoteAttachmentInline
    ]


admin.site.register(Note, NoteAdmin)
