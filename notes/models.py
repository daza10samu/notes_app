from django.db import models


class Note(models.Model):
    text = models.TextField()

    def __str__(self) -> str:
        return self.text


class Attachment(models.Model):
    note = models.ForeignKey(Note, on_delete=models.CASCADE, related_name="attachments")
    attachment = models.FileField()
